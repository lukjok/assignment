import React, { Component } from 'react';
import { Route } from 'react-router';
import { Layout } from './Pages/Shared/Layout';
import { Home } from './Pages/Home/Home';
import { ImportPage } from './Pages/Import/Import';

export default class App extends Component {
  static displayName = App.name;

  render () {
    return (
      <Layout>
        <Route exact path='/' component={Home} />
        <Route path='/import' component={ImportPage} />
      </Layout>
    );
  }
}
