import React, { Component } from 'react';

export class Loader extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        if (this.props.loading) {
            return (
                <div className="spinner-border text-primary ml-auto" role="status">
                    <span className="sr-only">Loading...</span>
                </div>
            );
        }
        else return null;
    }
}