class CounterPartyService {
    
    /**
     * Get a oaginated list of Counter Parties
     * @param {number} skipCount Number of items to skip
     * @param {number} maxResultCount Number of items to return
     */
    async getCounterPartiesAsync(skipCount, maxResultCount) {
        const response = await fetch(`api/CounterParty/List/${skipCount}/${maxResultCount}`)
            .catch((error) => {
                console.error('There has been a problem with your fetch operation:', error);
            });

        return await response.json();
    }

    /**
     * Uploads and imports data file to the system
     * @param {File} file Form file
     */
    async importAsync(file) {
        const formData = new FormData();
        formData.append('file', file);

        const response = await fetch('api/CounterParty/Import', {
            method: 'POST',
            body: formData
        })
            .catch((error) => {
                console.error('There has been a problem with your fetch operation:', error);
            });

        return response.ok;
    }
}
export default CounterPartyService;