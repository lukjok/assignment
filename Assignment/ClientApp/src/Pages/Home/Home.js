import React, { Component } from 'react';
import CounterPartyService from '../../services/counterPartyService'
import { Loader } from '../../components/Loader/Loader';
import ReactPaginate from 'react-paginate';
import './Home.css';

export class Home extends Component {
  static displayName = Home.name;

  constructor(props) {
    super(props);
    this.counterPartyService = new CounterPartyService();
    this.state = {
      data: [],
      loading: false,
      skipCount: 0,
      maxResultCount: 10,
      totalCount: 0
    };
  }

  /**
   * Get Counter Party list and save it to the state
   */
  getCounterPartyList() {
    this.setState({ loading: true });
    this.counterPartyService.getCounterPartiesAsync(this.state.skipCount, this.state.maxResultCount)
      .then(data => {
        this.setState({
          data: data.items,
          totalCount: data.totalCount,
          loading: false
        });
      }
      );
  }

  //Pagination click handler
  handlePageClick = data => {
    let selected = data.selected;
    let offset = selected * this.state.maxResultCount;

    this.setState({ skipCount: offset }, () => {
      this.getCounterPartyList();
    });
  };

  componentDidMount() {
    this.getCounterPartyList();
  }

  /**
   * Generates a Counter Party table from the provided array
   * @param {Array} data 
   */
   renderCounterPartyTable(data) {
    return (
      <table className="table">
        <thead>
          <tr>
            <th scope="col">ID</th>
            <th scope="col">Name</th>
            <th scope="col">Phone</th>
            <th scope="col">Fax</th>
            <th scope="col">Is Buyer</th>
            <th scope="col">Is Seller</th>
          </tr>
        </thead>
        <tbody>
          {data.map(function (item, index) {
            return <tr key={index}>
              <td>{item.counterPartyId}</td>
              <td>{item.name}</td>
              <td>{item.phone}</td>
              <td>{item.fax}</td>
              <td>{item.isBuyer ? "Yes" : "No"}</td>
              <td>{item.isSeller ? "Yes" : "No"}</td>
            </tr>;
          })}
        </tbody>
      </table>
    );
  }

  render() {
    return (
      <div>
        <div className="d-flex mb-2" style={{justifyContent: 'flex-end'}}>
          <Loader loading={this.state.loading} />
        </div>
        <div>
          {this.renderCounterPartyTable(this.state.data)}
          <ReactPaginate
            previousLabel={'previous'}
            nextLabel={'next'}
            breakLabel={'...'}
            breakClassName={'page-item'}
            breakLinkClassName={'page-link'}
            pageClassName={'page-item'}
            pageLinkClassName={'page-link'}
            previousClassName={'page-item'}
            previousLinkClassName={'page-link'}
            nextClassName={'page-item'}
            nextLinkClassName={'page-link'}
            pageCount={this.state.totalCount / this.state.maxResultCount}
            marginPagesDisplayed={2}
            pageRangeDisplayed={5}
            onPageChange={this.handlePageClick}
            containerClassName={'pagination'}
            subContainerClassName={'pages pagination'}
            activeClassName={'active'}
          />
        </div>
      </div>
    );
  }
}
