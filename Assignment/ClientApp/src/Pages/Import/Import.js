import React, { Component } from 'react';
import CounterPartyService from '../../services/counterPartyService'
import { Loader } from '../../components/Loader/Loader';

export class ImportPage extends Component {
  static displayName = ImportPage.name;

  constructor(props) {
    super(props);
    this.state = { loading: false, success: true, showMsg: false };
    this.counterPartyService = new CounterPartyService();
  }

  //Form change event handler
  onChangeHandler = event => {
    this.setState({
      loading: true
    });

    this.counterPartyService.importAsync(event.target.files[0])
      .then((result) => {
        this.setState({
          loading: false,
          success: result,
          showMsg: true
        });
      });
  }

  /**
   * Genrates a response message about import operation
   * @param {boolean} success Was action successful
   * @param {boolean} show Is message required
   */
   renderResponseMsg(success, show) {
    if (show) {
      return !success ?
        <div class="alert alert-danger" role="alert">
          Failed to import provided file
  </div> : <div class="alert alert-success" role="alert">
          Import was successful
  </div>;
    }
    else return null;
  }

  render() {
    return (
      <div>
        {this.renderResponseMsg(this.state.success, this.state.showMsg)}
        <div className="card">
          <div className="card-body">
            <div className="d-flex mb-2 align-items-center">
              <h5 className="card-title">Import counter parties</h5>
              <Loader loading={this.state.loading} />
            </div>
            <h6 className="card-subtitle mb-2 text-muted">Select file to import data</h6>
            <div className="card-text">
              <form>
                <div className="form-group">
                  <input type="file" className="form-control-file" onChange={this.onChangeHandler} id="fileUpload" />
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
