﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Assignment.Repositories.CounterParty
{
    public interface ICounterPartyRepository
    {
        Task AddAsync(EF.Entities.CounterParty counterParty);
        Task AddRangeAsync(List<EF.Entities.CounterParty> counterParties);
        Task<int> CountAsync();
        Task<List<EF.Entities.CounterParty>> GetListAsync(int skipCount, int maxResultCount);
    }
}
