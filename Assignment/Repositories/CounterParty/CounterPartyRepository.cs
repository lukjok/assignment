﻿using Assignment.EF;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Assignment.Repositories.CounterParty
{
    public class CounterPartyRepository : ICounterPartyRepository
    {
        private readonly AssignmentDbContext _context;

        public CounterPartyRepository(AssignmentDbContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Add provided Counter Party to the database
        /// </summary>
        /// <param name="counterParty">Counter Party</param>
        /// <returns></returns>
        public async Task AddAsync(EF.Entities.CounterParty counterParty)
        {
            _context.Companies.Add(counterParty);
            await _context.SaveChangesAsync();
        }

        /// <summary>
        /// Add a list of Counter Party to the database
        /// </summary>
        /// <param name="counterParties">List of Cuunter Parties</param>
        /// <returns></returns>
        public async Task AddRangeAsync(List<EF.Entities.CounterParty> counterParties)
        {
            _context.Companies.AddRange(counterParties);
            await _context.SaveChangesAsync();
        }

        /// <summary>
        /// Get count of Counter Parties
        /// </summary>
        /// <returns>Count of Counter Parties</returns>
        public async Task<int> CountAsync()
        {
            return await _context.Companies.CountAsync();
        }

        /// <summary>
        /// Get a paginated list of Counter Parties
        /// </summary>
        /// <param name="skipCount">Number of items to skip</param>
        /// <param name="maxResultCount">Number of items to return</param>
        /// <returns>List of Counter Parties</returns>
        public async Task<List<EF.Entities.CounterParty>> GetListAsync(int skipCount, int maxResultCount)
        {
            return await _context.Companies
                .AsNoTracking()
                .Skip(skipCount)
                .Take(maxResultCount)
                .ToListAsync();
        }
    }
}
