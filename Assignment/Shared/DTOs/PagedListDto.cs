﻿using System.Collections.Generic;

namespace Assignment.Shared.DTOs
{
    public class PagedListDto<T>
    {
        public PagedListDto(int totalCount, int maxResultCount, int skipCount, List<T> items)
        {
            TotalCount = totalCount;
            MaxResultCount = maxResultCount;
            SkipCount = skipCount;
            Items = items;
        }

        public int TotalCount { get; set; }
        public int MaxResultCount { get; set; }
        public int SkipCount { get; set; }

        public List<T> Items { get; set; }
    }
}
