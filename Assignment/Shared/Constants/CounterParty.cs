﻿namespace Assignment.Shared.Constants
{
    public static class CounterParty
    {
        public const int MaxCounterPartyIdLength = 50;
        public const int MaxPhoneLength = 50;
        public const int MaxFaxLength = 50;
    }
}
