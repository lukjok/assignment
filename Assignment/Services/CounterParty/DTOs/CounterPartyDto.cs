﻿namespace Assignment.Services.CounterParty.DTOs
{
    public class CounterPartyDto
    {
        public string CounterPartyId { get; set; }

        public string Name { get; set; }

        public string Phone { get; set; }

        public string Fax { get; set; }

        public bool IsBuyer { get; set; }

        public bool IsSeller { get; set; }
    }
}
