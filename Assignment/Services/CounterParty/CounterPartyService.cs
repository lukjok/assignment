﻿using Assignment.Repositories.CounterParty;
using Assignment.Services.CounterParty.DTOs;
using Assignment.Shared.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Assignment.Services.CounterParty
{
    public class CounterPartyService : ICounterPartyService
    {
        private readonly ICounterPartyRepository _repository;

        public CounterPartyService(ICounterPartyRepository repository)
        {
            _repository = repository;
        }

        /// <summary>
        /// Add provided Counter Party to the system
        /// </summary>
        /// <param name="counterParty">Counter Party</param>
        /// <returns></returns>
        public async Task AddAsync(EF.Entities.CounterParty counterParty)
        {
            await _repository.AddAsync(counterParty);
        }

        /// <summary>
        /// Add a list of Counter Party to the system
        /// </summary>
        /// <param name="counterParties">List of Cuunter Parties</param>
        /// <returns></returns>
        public async Task AddRangeAsync(List<EF.Entities.CounterParty> counterParties)
        {
            await _repository.AddRangeAsync(counterParties);
        }

        /// <summary>
        /// Get a paginated list of Counter Parties
        /// </summary>
        /// <param name="skipCount">Number of items to skip</param>
        /// <param name="maxResultCount">Number of items to return</param>
        /// <returns>A paginated list of Counter Parties</returns>
        public async Task<PagedListDto<CounterPartyDto>> GetListAsync(int skipCount, int maxResultCount)
        {
            if (skipCount < 0)
                throw new ArgumentException("Number must have positive value", nameof(skipCount));
            if(maxResultCount < 0)
                throw new ArgumentException("Number must have positive value", nameof(maxResultCount));

            int totalCount = await _repository.CountAsync();
            List<EF.Entities.CounterParty> data = await _repository.GetListAsync(skipCount, maxResultCount);

            List<CounterPartyDto> counterParties = data.Select(x => new CounterPartyDto
            {
                CounterPartyId = x.CounterPartyId,
                Fax = x.Fax,
                Phone = x.Phone,
                Name = x.Name,
                IsBuyer = x.IsBuyer,
                IsSeller = x.IsSeller
            })
            .ToList();

            return new PagedListDto<CounterPartyDto>(totalCount, maxResultCount, skipCount, counterParties);
        }
    }
}
