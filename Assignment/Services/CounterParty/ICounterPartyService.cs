﻿using Assignment.Services.CounterParty.DTOs;
using Assignment.Shared.DTOs;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Assignment.Services.CounterParty
{
    public interface ICounterPartyService
    {
        Task AddAsync(EF.Entities.CounterParty counterParty);
        Task AddRangeAsync(List<EF.Entities.CounterParty> counterParties);
        Task<PagedListDto<CounterPartyDto>> GetListAsync(int skipCount, int maxResultCount);
    }
}
