﻿using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;

namespace Assignment.Services.CounterPartyImporter
{
    public interface ICounterPartyImporterService
    {
        Task ImportAsync(IFormFile file);
    }
}
