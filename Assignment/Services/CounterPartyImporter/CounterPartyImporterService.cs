﻿using Assignment.Repositories.CounterParty;
using Assignment.Utilities.CounterParty;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace Assignment.Services.CounterPartyImporter
{
    public class CounterPartyImporterService : ICounterPartyImporterService
    {
        private readonly ICounterPartyRepository _counterPartyRepository;
        private readonly CsvReader _csvReader;

        public CounterPartyImporterService(ICounterPartyRepository counterPartyRepository)
        {
            _counterPartyRepository = counterPartyRepository;
            _csvReader = new CsvReader();
        }

        /// <summary>
        /// Imports Counter Party data from uploaded file
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        public async Task ImportAsync(IFormFile file)
        {
            string fileExtension = Path.GetExtension(file.FileName);
            Stream stream = file.OpenReadStream();

            List<EF.Entities.CounterParty> counterParties = FormatCounterPartyList(stream, fileExtension);
            await _counterPartyRepository.AddRangeAsync(counterParties);
        }

        /// <summary>
        /// A general method to read data from provided stream according to specified file format
        /// </summary>
        /// <param name="input">File stream</param>
        /// <param name="fileExtension">File extension, e.g csv, xml</param>
        /// <returns>List of Counter Parties</returns>
        private List<EF.Entities.CounterParty> FormatCounterPartyList(Stream input, string fileExtension)
        {
            switch (fileExtension)
            {
                case ".csv":
                    return _csvReader.Process(input);
                case ".xml":
                    //Further implementation
                    return new List<EF.Entities.CounterParty>();
                default:
                    return new List<EF.Entities.CounterParty>();
            }
        }
    }
}
