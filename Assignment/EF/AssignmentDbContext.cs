﻿using Assignment.EF.Entities;
using Microsoft.EntityFrameworkCore;

namespace Assignment.EF
{
    public class AssignmentDbContext : DbContext
    {
        public AssignmentDbContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<CounterParty> Companies { get; set; }
    }
}
