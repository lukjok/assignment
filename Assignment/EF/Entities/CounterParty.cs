﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Assignment.EF.Entities
{
    public class CounterParty
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        [StringLength(Shared.Constants.CounterParty.MaxCounterPartyIdLength)]
        public string CounterPartyId { get; set; }

        public string Name { get; set; }

        [Required]
        [StringLength(Shared.Constants.CounterParty.MaxPhoneLength)]
        public string Phone { get; set; }

        [Required]
        [StringLength(Shared.Constants.CounterParty.MaxFaxLength)]
        public string Fax { get; set; }

        public bool IsBuyer { get; set; }

        public bool IsSeller { get; set; }
    }
}
