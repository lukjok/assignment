﻿using System.IO;

namespace Assignment.Utilities.Readers
{
    public interface IStreamReader<T>
    {
        T Process(Stream input);
    }
}
