﻿using Assignment.Utilities.Readers;
using System.Collections.Generic;
using System.IO;

namespace Assignment.Utilities.CounterParty
{
    public class CsvReader : IStreamReader<List<EF.Entities.CounterParty>>
    {
        /// <summary>
        /// Processes provided file stream and return read Counter Party-list
        /// </summary>
        /// <param name="input">File stream</param>
        /// <returns>List of Counter Parties</returns>
        public List<EF.Entities.CounterParty> Process(Stream input)
        {
            List<EF.Entities.CounterParty> data = new List<EF.Entities.CounterParty>();

            using (StreamReader sr = new StreamReader(input))
            {
                string currentLine = string.Empty;
                //Skip header and proceed to the actual data
                sr.ReadLine();

                while ((currentLine = sr.ReadLine()) != null)
                {
                    string[] parts = currentLine.Split(',');

                    //If CSV data are missing parts just skip it
                    if (parts.Length == 6)
                    {
                        data.Add(new EF.Entities.CounterParty
                        {
                            CounterPartyId = parts[0],
                            Name = parts[1],
                            IsBuyer = parts[2] == "Yes",
                            IsSeller = parts[3] == "Yes",
                            Phone = parts[4],
                            Fax = parts[5]
                        });
                    }
                }
            }

            return data;
        }
    }
}
