using Assignment.Services.CounterParty;
using Assignment.Services.CounterParty.DTOs;
using Assignment.Services.CounterPartyImporter;
using Assignment.Shared.DTOs;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Assignment.Controllers
{
    [Route("api/[controller]")]
    public class CounterPartyController : Controller
    {
        private readonly ICounterPartyService _counterPartyService;
        private readonly ICounterPartyImporterService _counterPartyImporterService;

        public CounterPartyController(ICounterPartyService counterPartyService, ICounterPartyImporterService counterPartyImporterService)
        {
            _counterPartyImporterService = counterPartyImporterService;
            _counterPartyService = counterPartyService;
        }

        /// <summary>
        /// Import provided Counter Party data file to the system
        /// </summary>
        /// <param name="file">Form file</param>
        /// <returns></returns>
        [HttpPost("Import")]
        public async Task ImportAsync(IFormFile file)
        {
            await _counterPartyImporterService.ImportAsync(file);
        }

        /// <summary>
        /// Get a paginated list of Counter Parties
        /// </summary>
        /// <param name="skipCount">Number of items to skip</param>
        /// <param name="maxResultCount">Number of items to return</param>
        /// <returns>A paginated list of Counter Parties</returns>
        [HttpGet("List/{skipCount}/{maxResultCount}")]
        public async Task<PagedListDto<CounterPartyDto>> GetListAsync(int skipCount, int maxResultCount)
        {
            return await _counterPartyService.GetListAsync(skipCount, maxResultCount);
        }        
    }
}
